import { AppModule } from "./app/app.module";
import { registerElement, platformNativeScriptDynamic } from "@nativescript/angular";

registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);
platformNativeScriptDynamic().bootstrapModule(AppModule);
