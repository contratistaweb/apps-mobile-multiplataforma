import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component';
import { HistoryComponent } from './history/history.component';
import { NativeScriptCommonModule } from '@nativescript/angular';


@NgModule({
  declarations: [
    AboutComponent,
    HistoryComponent
  ],
  imports: [
    AboutRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AboutModule { }
