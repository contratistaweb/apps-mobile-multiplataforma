import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { AboutComponent } from './about.component';
import { HistoryComponent } from './history/history.component';


const routes: Routes = [
  {
    path: '',
    component: AboutComponent
  },
  {
    path: 'history',
    component: HistoryComponent
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class AboutRoutingModule { }
